# Terms and Conditions

The Vision Critical Extractor for Keboola is built and offered by Martin Fiser (Fisa) as a third party component. It is provided as-is, without guarantees and support, and for no additional charge. 
You are free to use it or check out the [source code](https://bitbucket.org/VFisa/kbc_visioncritical_extractor/). Please contribute to the code and report any encountered bugs.
There are several extractor configurations built-in in the JSON format. 
API call is processed through KBC platform, so no sensitive information is being sent non-standard way, maintaining all Keboola recommended security standards along the way.

## Contact   
### Vision Critical API   
Vision Critical   
email: support@visioncritical.com    
web: https://www.visioncritical.com/trust/support/   


### Extractor
Fisa    
Martin Fiser  
Vancouver, Canada (PST time)   
email: VFisa.stuff@gmail.com   